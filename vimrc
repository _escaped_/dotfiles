set nocompatible               " be iMproved
filetype off                   " required!

set rtp+=~/.vim/bundle/Vundle.vim/
call vundle#begin()

" let Vundle manage Vundle
" required!
Plugin 'gmarik/vundle'

" color theme
Plugin 'morhetz/gruvbox'

" tmux support
Plugin 'christoomey/vim-tmux-navigator'
Plugin 'tpope/vim-obsession'
" auto disable search highlight
Plugin 'junegunn/vim-slash'
" disable arrow keys :)
Plugin 'mrmargolis/dogmatic.vim'

" file tree browser
Plugin 'scrooloose/nerdtree'
" quicksearch for files within project dir
Plugin 'kien/ctrlp.vim'
" integrate ack into vim
Plugin 'dyng/ctrlsf.vim'
" multiple cursors
Plugin 'terryma/vim-multiple-cursors'
" display git status (line added, changed, ..)
Plugin 'airblade/vim-gitgutter'
" beautiful status line
Plugin 'itchyny/lightline.vim'
" detect used indent (spaces, tabs, ..)
Plugin 'ogier/guessindent'

" Syntax checker
Plugin 'w0rp/ale'
" autocomplet
Plugin 'Valloric/YouCompleteMe'
" special python support
Plugin 'davidhalter/jedi-vim'
Plugin 'fisadev/vim-isort'

" hightlight
Plugin 'sheerun/vim-polyglot'

" enable repeating supported plugin maps with '.'
Plugin 'tpope/vim-repeat'
" comments
Plugin 'tpope/vim-commentary'

" auto close all kind of brackets, ...
Plugin 'Raimondi/delimitMate'

" git integration
Plugin 'tpope/vim-fugitive'

" markdown preview
Plugin 'previm/previm'
Plugin 'tyru/open-browser.vim'

call vundle#end()


set backupdir=~/.vim/sessions
set dir=~/.vim/sessions


" ==========================================================
" basic vim config
" ==========================================================
syntax on                   " syntax highlighing
filetype on                 " try to detect filetypes
filetype plugin indent on   " enable loading indent file for filetype
set encoding=utf-8
if has("gui_running")
    set guifont=Source\ Code\ Pro\ for\ Powerline\ Light\ 10
endif

""" Command Line
set wildmenu                " Autocomplete features in the status bar
set wildmode=longest,list,full
set wildignore=*.o,*.obj,*.bak,*.exe,*.py[co],*.swp,*~,*.pyc,.svn

"" Insert completion
" don't select first item, follow typing in autocomplete
"set completeopt=menuone,longest,preview
"set pumheight=6             " Keep a small completion window"

""" Searching and Patterns
set ignorecase              " Default to using case insensitive searches,
set smartcase               " unless uppercase letters are used in the regex.
set hlsearch                " Highlight searches by default.
set incsearch               " Incrementally search while typing a /regex

""" Display
set laststatus=2            " Enables the status line at the bottom of Vim"
set lazyredraw              " Don't repaint when scripts are running
set scrolloff=3             " Keep 3 lines below and above the cursor
set cursorline              " have a line indicate the cursor location
set ruler                   " line numbers and column the cursor is on
set nowrap                  " don't wrap text
set linebreak               " don't wrap textin the middle of a word
set number                  " Show line numbering
set numberwidth=2           " Use 1 col + 1 space for numbers
set colorcolumn=80,90,120   " show verticle bar after 80 chars
set list
set list listchars=tab:→·,trail:·,eol:¬,extends:⇉,precedes:⇇

""" Set autoindent
set autoindent              " always set autoindenting on
set smartindent             " use smart indent if there is no indent file
set tabstop=4               " <tab> inserts 4 spaces
set shiftwidth=4            " but an indent level is 2 spaces wide.
set softtabstop=4           " <BS> over an autoindent deletes both spaces.
set expandtab               " Use spaces, not tabs, for autoindent/tab key.
set shiftround              " rounds indent to a multiple of shiftwidth

""" always use clipboard
set clipboard=unnamed,unnamedplus


""" backspace should remove nl, chars, ..  (http://vi.stackexchange.com/a/2163)
set backspace=indent,eol,start


" ===========================================================
" FileType specific changes
" ============================================================
" only 2 spaces for some files
au FileType html,xhtml,xml,css,coffee,sass,scss,less,djangohtml
    \ setl expandtab shiftwidth=2 tabstop=2 softtabstop=2

" load closetag only for html/xml like files
au FileType html,htmldjango,jinjahtml,eruby,mako let b:closetag_html_style=1
au FileType html,xhtml,xml,htmldjango,jinjahtml,eruby,mako
    \ source ~/.vim/bundle/closetag.vim/plugin/closetag.vim

" In plain-text files and svn commit buffers, wrap automatically at 78 chars
au FileType text setlocal tw=78 fo+=t

au BufRead,BufNewFile *.dart set filetype=dart
au BufRead,BufNewFile *.md set filetype=markdown

" ==========================================================
" lightline
" ==========================================================

let g:lightline = {
  \ 'component_function': {
    \ 'filename': 'LightlineFilename',
  \ },
\ }

function! LightlineFilename()
  return expand('%:t') ==# '' ? '[No Name]' : pathshorten(fnamemodify(expand('%'), ":."))
endfunction


" ==========================================================
" ctrlp
" ==========================================================
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\.(git|hg|svn)$\|coverage$\|htmlcov$\|node_modules$\|bower_components$',
  \ 'file': '\v\.(exe|so|dll|sql|orig|pyc|jpg|png|pdf|mp4|webm)$',
  \ }


" ==========================================================
" ale
" ==========================================================
let g:ale_sign_column_always = 1  " keep the sign guttern
let g:ale_keep_list_window_open = 0  " do not keep loclist open if there is no error
let g:ale_open_list = 1  " ale should open the loclist


" ==========================================================
" completion
" ==========================================================
let g:jedi#completions_enabled = 0

" Tab completion
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<cr>"

" Force refresh completion
imap <c-space> <Plug>(asyncomplete_force_refresh)

" To auto close preview window when completion is done.
autocmd! CompleteDone * if pumvisible() == 0 | pclose | endif


" ==========================================================
" DetectIndent
" ==========================================================
" autocmd BufReadPost * :GuessIndent


" ==========================================================
" Nerdtree
" ==========================================================
" automatically open nerdtree
" autocmd vimenter * NERDTree
let NERDTreeIgnore = ['\.pyc$', '\.pyo$', '__pycache__']


" ==========================================================
" Theme
" ==========================================================
set background=dark         " I use dark background
" call togglebg#map("<F5>")   " toggle bg color
" stop Solarized from displaying bold, underlined or italicized typefaces
let g:solarized_italic=0
let g:solarized_termtrans=1
let g:solarized_termcolors=256
let g:solarized_contrast="normal"
let g:solarized_visibility="normal"

let g:gruvbox_contrast_dark="medium"
let g:gruvbox_contrast_light="medium"
colorscheme gruvbox


" ==========================================================
" Basic keybindings
" ==========================================================
let mapleader=","

" ctrl-jklm changes to that split
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h

" Seriously, guys. It's not like :W is bound to anything anyway.
command! W :w

" sudo write this
cmap W! w !sudo tee % >/dev/null
cmap w!! w !sudo tee % >/dev/null

" hide matches on <leader>space
nnoremap <leader><space> :nohlsearch<cr>

" Remove trailing whitespace on <leader>S
nnoremap <leader>S :%s/\s\+$//<cr>:let @/=''<CR>

" Open NerdTree
map <leader>n :NERDTreeToggle<CR>

" ctrlsf
nmap <c-f>f <Plug>CtrlSFPrompt
