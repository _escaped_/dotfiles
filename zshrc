## configure theme: pure
autoload -U promptinit && promptinit
PURE_CMD_MAX_EXEC_TIME=10
PURE_PROMPT_SYMBOL=$


# Check if zplug is installed
if [[ ! -d ~/.zplug ]]; then
  git clone https://github.com/zplug/zplug ~/.zplug
  source ~/.zplug/init.zsh && zplug update --self
fi
export ZPLUG_HOME="${HOME}/.zplug"
source "${ZPLUG_HOME}/init.zsh"

# manage itself
zplug "zplug/zplug"

# theme: pure
zplug "mafredri/zsh-async"
zplug "sindresorhus/pure"

# completions
zplug "zsh-users/zsh-completions"
# syntax highlighting
zplug "zsh-users/zsh-syntax-highlighting"
# history substring search
zplug "zsh-users/zsh-history-substring-search"
# autocompletion
zplug "zsh-users/zsh-autosuggestions"
# vim bindings
zplug "b4b4r07/zsh-vimode-visual"

# colors
zplug "morhetz/gruvbox", use:gruvbox_256palette.sh

# Adds node_modules/.bin to the PATH
zplug "puffnfresh/4151775", \
    from:gist, \
    as:plugin, \
    use:"*.sh"

# gbrt
zplug "trey/2322304", \
    from:gist, \
    as:command, \
    use:"*.sh", \
    rename-to:"gbrt"

# z is the new j, yo
zplug "rupa/z", \
    as:command, \
    use:"z.sh"

# tldr
zplug "raylee/tldr", \
    as:command, \
    use:"tldr"

# scmpuff
zplug "mroth/scmpuff", \
    from:gh-r, \
    as:command, \
    rename-to:scmpuff, \
    use:"*linux*64*"

# diff-so-fancy
zplug "so-fancy/diff-so-fancy", \
    as:command, \
    use:"diff-so-fancy"
zplug "so-fancy/diff-so-fancy", \
    as:command, \
    use:"third_party/diff-highlight/diff-highlight"

# fuzzy file finder: fzf
zplug "junegunn/fzf-bin", \
    from:gh-r, \
    as:command, \
    use:"*linux*amd64*", \
    rename-to:fzf

# git
zplug "git-ftp/git-ftp", \
    as:command, \
    use:"git-ftp"
zplug "PotatoLabs/git-redate", \
    as:command, \
    use:"git-redate"

# Parsing HTML at the command line
zplug "ericchiang/pup", \
    from:gh-r, \
    as:command, \
    rename-to:pup, \
    use:"*linux*amd64*"

# local plugins
zplug "~/.zsh", from:local


# Install plugins if there are plugins that have not been installed
if ! zplug check --verbose; then
    printf "Install? [y/N]: "
    if read -q; then
        echo; zplug install
    fi
fi

# Then, source plugins and add commands to $PATH
zplug load --verbose


## setup colors
alias ls='ls --color=auto'
alias grep='grep --color=always'

if whence guake &>/dev/null; then
    # dark
    #gconftool-2 -s -t string /apps/guake/style/font/color "#EAEADBDBB1B1"
    #gconftool-2 -s -t string /apps/guake/style/background/color "#272727272727"
    #gconftool-2 -s -t string /apps/guake/style/font/palette "#272727272727:#CCCC23231C1C:#989897971919:#D7D799992020:#454584848787:#B1B161618585:#68689D9D6A6A:#BCBCAEAE9292:#7C7C6F6F6363:#FBFB49493434:#B7B7BABA2525:#F9F9BCBC2E2E:#8282A5A59898:#D3D385859B9B:#8D8DBFBF7C7C:#EAEADBDBB1B1"
    # light
    gconftool-2 -s -t string /apps/guake/style/font/color "#3C3C38383636"
    gconftool-2 -s -t string /apps/guake/style/background/color "#FCFCF3F3C0C0"
    gconftool-2 -s -t string /apps/guake/style/font/palette "#FCFCF3F3C0C0:#CCCC23231C1C:#989897971919:#D7D799992020:#454584848787:#B1B161618585:#68689D9D6A6A:#66665B5B5353:#A8A899998383:#9D9D00000606:#797974740D0D:#B4B476761414:#070766667878:#8E8E3F3F7171:#42427B7B5757:#3C3C38383636"
fi


## history-substring-search
# bind UP and DOWN arrow keys
zmodload zsh/terminfo
bindkey "$terminfo[kcuu1]" history-substring-search-up
bindkey "$terminfo[kcud1]" history-substring-search-down


## autocompletion
zstyle :compinstall filename '$HOME/.zshrc'
autoload -Uz compinit
compinit
zstyle ':completion:*' menu select


## history
HISTFILE=~/.zsh_history
HISTSIZE=9000
SAVEHIST=9000
setopt hist_ignore_all_dups
setopt append_history
setopt share_history
setopt hist_reduce_blanks
setopt inc_append_history
unsetopt extended_history


## miscellaneous
setopt autocd
unsetopt beep
unsetopt extendedglob


## aliases
export EDITOR='vim'
# human readable sizes
alias df='df -h'
alias du='du -h'


# load rbenv if available
if which rbenv &>/dev/null; then
  eval "$(rbenv init - --no-rehash)"
fi

# init scmpuff
if zplug check mroth/scmpuff; then
  eval "$(scmpuff init -s)"
fi

# load NVM if available
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && source "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

if which nvm &>/dev/null; then
  if ! which npm &>/dev/null; then
    nvm install --lts
    nvm alias default lts/*
  fi

  # autoload required node version
  autoload -U add-zsh-hook
  load-nvmrc() {
    local node_version="$(nvm version)"
    local nvmrc_path="$(nvm_find_nvmrc)"

    if [ -n "$nvmrc_path" ]; then
      local nvmrc_node_version=$(nvm version "$(cat "${nvmrc_path}")")

      if [ "$nvmrc_node_version" = "N/A" ]; then
        nvm install
      elif [ "$nvmrc_node_version" != "$node_version" ]; then
        nvm use
      fi
    elif [ "$node_version" != "$(nvm version default)" ]; then
      echo "Reverting to nvm default version"
      nvm use default
    fi
  }
  add-zsh-hook chpwd load-nvmrc
  load-nvmrc
fi

## PATH
export PATH
path=(
  ~/.bin
  ~/.local/bin/
  ~/.npm-packages/bin
  dirname `nvm which current`
  ~/.gem/ruby/*/bin(Nn[-1])
  ~/.cargo/bin
  ~/.pyenv/bin
  $path[@]
)
path=(${(u)^path:A}(N-/))

# load pyenv
if which pyenv &>/dev/null; then
    export PYTHON_CONFIGURE_OPTS="--enable-shared --enable-loadable-sqlite-extensions"
    export PYENV_ROOT="$HOME/.pyenv"
    export PATH="$HOME/.pyenv/bin:$PATH"
    eval "$(pyenv init -)"
fi

# capslock as ESC
if which setxkbmap &>/dev/null; then
  setxkbmap -option caps:escape;
fi

# requires PATH already updated
if ! which pipx &>/dev/null; then
  python3 -m pip install --user pipx
  python3 -m pipx ensurepath
fi

if which pipx &>/dev/null; then
  # install tox
  if ! which tox &>/dev/null; then
    pipx install tox
  fi

  # install poetry
  if ! which poetry &>/dev/null; then
    pipx install poetry
    poetry config settings.virtualenvs.in-project true
  fi

  # install pipenv
  if ! which pipenv &>/dev/null; then
    pipx install pipenv
  fi

  # install howdoi
  if ! which howdoi &>/dev/null; then
    pipx install howdoi
  fi

  # install pre-commit
  if ! which pre-commit &>/dev/null; then
    pipx install pre-commit
  fi

  # install docker-compose
  if ! which docker-compose &>/dev/null; then
    pipx install docker-compose
  fi

  # init packages
fi

# set some alias
alias dc="docker-compose"
