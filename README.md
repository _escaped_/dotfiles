# escaped's dotfiles
These dotfiles are managed using `https://github.com/jbernard/dotfiles`.

## Install
Clone this repository including its submodules into `~/.dotfiles`

    git clone --recusive <repo-url> ~/.dotfiles

and install them.

    ~/.dotfiles/bin/dotfiles -s --force -R ~/.dotfiles -C ~/.dotfiles/dotfilesrc


## Post Install

Update locale font cache

    fc-cache -f ~/.fonts

Install vim plugins

    vim +PluginInstall +qall




To add or remove files from the `.dotfiles` directories,
use the `dotfiles` executable without any special arguments.
Do not forget to commit your changes.
