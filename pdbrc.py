try:
    from pdb import DefaultConfig
except ImportError:
    pass
else:
    class Config(DefaultConfig):
        sticky_by_default = True
